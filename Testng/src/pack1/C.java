package pack1;

import org.testng.annotations.Test;

public class C {
	@Test(priority=2)
	public void one() {
		System.out.println("This is First Test case Number");
		}
	@Test(priority=0,enabled=false)
	public void two() {
		System.out.println("This is Second Test case Number");
	}
	@Test(priority=3)
	public void three() {
		System.out.println("This is Third Test case Number");
	}
	@Test(priority=1,enabled=false)
	public void Four() {
		System.out.println("This is Fourth Test case Number");
}
}

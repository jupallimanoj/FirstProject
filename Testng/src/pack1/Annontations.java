package pack1;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Annontations {

	@Test
	public void test1() {
		System.out.println("This is First Test Case");
	}

	@Test
	public void test2() {
		System.out.println("This is Second Test Case");
	}

	@BeforeMethod()
	public void beforeMethod() {
		System.out.println("This Will Execute Before Every Method");

	}

	@AfterMethod()
	public void afterMethod() {
		System.out.println("This will Execute After Every Method");
	}

	@BeforeClass()
	public void beforeClass() {
		System.out.println("This will execute Before Every Class");
	}

	@AfterClass()
	public void afterClass() {
		System.out.println("This Will Execute After Every Class");
	}

	@BeforeTest()
	public void beforeTest() {
		System.out.println("This will Execute Before Every Test ");
	}

	@AfterTest()
	public void afterTest() {
		System.out.println("This Will Excute After Every Test ");
	}

	@BeforeSuite()
	public void beforeSuite() {
		System.out.println("This will Execute Before Every Suite");
	}

	@AfterTest()
	public void afterSuite() {
		System.out.println("This will Execute After Every Test");
	}

}

package pack1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MultiBrowser {

	public WebDriver driver;

	@Parameters("browser")

	@BeforeClass

	public void beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {

			/*System.setProperty("webdriver.gecko.driver",
					"D:\\Automation Softwares\\Automationworkspace\\Testng\\Drivers\\geckodriver.exe");
			driver = new FirefoxDriver();*/

		} else if (browser.equalsIgnoreCase("chrome")) {

			
			 System.setProperty("webdriver.chrome.driver", "D:\\Automation Softwares\\Automationworkspace\\Testng\\Drivers\\chromedriver.exe");
			 
			 
			 
			driver = new ChromeDriver();

		}

		driver.get("https://www.facebook.com/");

	}

	@Test
	public void login() throws InterruptedException {

		Thread.sleep(2000);

		

		driver.findElement(By.xpath(".//*[@id='u_0_g']")).sendKeys("Manoj");

		driver.findElement(By.xpath(".//*[@id='u_0_i']")).sendKeys("Jupalli");
		
		driver.findElement(By.xpath(".//*[@id='u_0_y']")).click();


	}

	@AfterClass
	public void afterTest() {

		driver.quit();

	}

}

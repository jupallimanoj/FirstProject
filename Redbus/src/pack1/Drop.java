package pack1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class Drop {

	public void m() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"D:\\Automation Softwares\\Automationworkspace\\Redbus\\Drivers\\chromedriver.exe");

		WebDriver dr = new ChromeDriver();

		dr.manage().window().maximize();

		dr.get("https://facebook.com");

		WebElement dt = dr.findElement(By.xpath(".//*[@id='day']"));

		Select ss = new Select(dt);
		ss.selectByVisibleText("26");

		WebElement mn = dr.findElement(By.xpath(".//*[@id='month']"));

		Select mm = new Select(mn);
		mm.selectByIndex(8);
		
		WebElement yy=dr.findElement(By.xpath(".//*[@id='year']"));
		
		Select ye=new Select(yy);
		ye.selectByIndex(5);
		
		Thread.sleep(6000);
		
		dr.close(); 

	}

	@Test
	public void m1() throws InterruptedException {
		m();
	}
}

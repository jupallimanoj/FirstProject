package pack1;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.remote.server.handler.WebDriverHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class B {

	public void m() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"D:\\Automation Softwares\\Automationworkspace\\Redbus\\Drivers\\chromedriver.exe");

		WebDriver dr = new ChromeDriver();

		dr.manage().window().maximize();

		dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		dr.get("https://www.redbus.in/");

		dr.findElement(By.xpath(".//*[@id='sign-in-icon-down']")).click();

		dr.findElement(By.xpath(".//*[@id='signInLink']")).click();

		WebDriverWait wait = new WebDriverWait(dr, 10);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(2));
		
		WebElement signin = dr.findElement(By.xpath(".//*[@id='g-signin2']/div/div"));
        signin.click();
		WebDriverWait w = new WebDriverWait(dr, 15);
		w.until(ExpectedConditions.visibilityOf(signin));

		signin.click();
		Thread.sleep(3000);

		String parentwindow = dr.getWindowHandle();
		System.out.println(parentwindow);

		Set<String> allwindows = dr.getWindowHandles();

		dr.switchTo().defaultContent();

		Thread.sleep(2000);

		for (String s : allwindows) {
			System.out.println(s);
			if (!parentwindow.equals(s)) {
				dr.switchTo().window(s);
				dr.manage().window().maximize();
				//cd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				String currentwindowhashcode = dr.getWindowHandle();
				System.out.println(parentwindow + "   " + currentwindowhashcode);
				dr.findElement(By.id("identifierId")).sendKeys("9676889924");

			}}
	}
	
@Test
public void m1() throws InterruptedException {
	m();
}
}
